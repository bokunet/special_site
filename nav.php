<nav>
	<div id="close">
		<button onClick="display()">閉じる</button>
	</div>
	<ul class="find" id="booth">
		<li class="category">
			<ul>
				<li>
					<a href="<?php echo $path;?>booth/food" title="食から探す 電波学園祭2017">
						<picture>
							<source type="image/webp" srcset="<?php echo $path;?>img/food.webp">
							<img src="<?php echo $path;?>img/food.png" alt="食から探す">
						</picture>
					</a>
				</li>
				<li>
					<a href="<?php echo $path;?>booth/learn" title="学から探す 電波学園祭2017">
						<picture>
							<source type="image/webp" srcset="<?php echo $path;?>img/learn.webp">
							<img src="<?php echo $path;?>img/learn.png" alt="学から探す">
						</picture>
					</a>
				</li>
				<li>
					<a href="<?php echo $path;?>booth/play" title="娯から探す 電波学園祭2017">
						<picture>
							<source type="image/webp" srcset="<?php echo $path;?>img/play.webp">
							<img src="<?php echo $path;?>img/play.png" alt="娯から探す">
						</picture>
					</a>
				</li>
			</ul>
		</li>
		<li class="class">
			<ul>
				<li>
					<a href="<?php echo $path;?>booth/med" title="先端医療系 電波学園祭2017">
						<picture>
							<source type="image/webp" srcset="<?php echo $path;?>img/med.webp">
							<img src="<?php echo $path;?>img/med.png" alt="先端医療系">
						</picture>
					</a>
				</li>
				<li>
					<a href="<?php echo $path;?>booth/info" title="先端技術系 電波学園祭2017">
						<picture>
							<source type="image/webp" srcset="<?php echo $path;?>img/info.webp">
							<img src="<?php echo $path;?>img/info.png" alt="先端技術系">
						</picture>
					</a>
				</li>
				<li>
					<a href="<?php echo $path;?>booth/electro" title="電子・電気系 電波学園祭2017">
						<picture>
							<source type="image/webp" srcset="<?php echo $path;?>img/electro.webp">
							<img src="<?php echo $path;?>img/electro.png" alt="電子・電気系">
						</picture>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<ul class="main_nav">
		<li><a href="<?php echo $path;?>">HOME</a></li>
		<li><button onClick="display()">ブース</button></li>
		<!--<li><a href="#">NEWS</a></li>-->
		<!--<li><a href="#">学内<br>マップ</a></li>-->
		<li><a href="<?php echo $path;?>access">アクセス</a></li>
	</ul>
</nav>