<header>
	<h1>
		<a href="<?php echo $path;?>">
			<picture>
				<source type="image/webp" srcset="<?php echo $path;?>img/header.webp">
				<img src="<?php echo $path;?>img/header.png" alt="東京電子専門学校 電波学園祭2017 ようこそTECパークへ">
			</picture>
			<img src="<?php echo $path;?>img/gear.gif" alt="歯車" id="gear">
		</a>
	</h1>
</header>
<?php include('share.php');?>
<div class="swiper-container">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<picture>
				<source type="image/webp" srcset="<?php echo $path;?>img/slide/slide1.webp">
				<img class="caption" src="<?php echo $path;?>img/slide/slide1.jpg">
			</picture>
		</div>
		<div class="swiper-slide">
			<picture>
				<source type="image/webp" srcset="<?php echo $path;?>img/slide/slide2.webp">
				<img class="caption" src="<?php echo $path;?>img/slide/slide2.jpg">
			</picture>
		</div>
		<div class="swiper-slide">
			<picture>
				<source type="image/webp" srcset="<?php echo $path;?>img/slide/slide3.webp">
				<img class="caption" src="<?php echo $path;?>img/slide/slide3.jpg">
			</picture>
		</div>
		<div class="swiper-slide">
			<picture>
				<source type="image/webp" srcset="<?php echo $path;?>img/slide/slide4.webp">
				<img class="caption" src="<?php echo $path;?>img/slide/slide4.jpg">
			</picture>
		</div>
		<div class="swiper-slide">
			<picture>
				<source type="image/webp" srcset="<?php echo $path;?>img/slide/slide5.webp">
				<img class="caption" src="<?php echo $path;?>img/slide/slide5.jpg">
			</picture>
		</div>
	</div>
	<div class="swiper-pagination"></div>
	<div class="date">
		<img src="<?php echo $path;?>img/date .png" alt="2017年10月28日（土）29（日）10:00～16:00">
	</div>
</div>