<?php
session_start();
session_regenerate_id( true );
include_once( 'path.php' );
$request = array_values( array_filter( explode( '/', $_GET[ 'request' ] ), "strlen" ) );
$title = '';
$description = "";
if ( $request == [] ) {
	//xamppのnotice回避用
} elseif ( $request[ 0 ] == 'access' ) {
	$title = '学校までのアクセス';
} elseif ( $request[ 0 ] == 'archives' ) {
	$title = '過去のパンフレット一覧';
} elseif ( $request[ 0 ] == 'about' ) {
	$title = 'このサイトについて';
} elseif ( $request[ 0 ] == 'postercontest' ) {
	$title = 'ポスターコンテスト';
} elseif ( $request[ 0 ] == 'floor' ) {
	$title = '学内マップ';
} elseif ( $request[ 0 ] == 'booth' ) {
	$title = 'ブースを探す';
}

if ( $title != '' ) {
	$title = $title . ' | ';
}
$description .= "東京電子専門学校の学園祭'電波学園祭'の2017年特設サイト！ 今年度は10月28日（土）と29日（日）に開催！";
?>
<!DOCTYPE html>
<html lang="ja">

<head>
	<?php include_once('head.php');?>
</head>

<body>
	<?php include_once('nav.php');?>
	<div class="wrapper">
		<?php include_once('header.php');?>
		<?php if($request==[]){//index ?>
		<ul class="find">
			<li>
				<h2>
					<picture>
						<source type="image/webp" srcset="img/find_caption.webp">
						<img class="caption" src="img/find_caption.png" alt="ブースをさがす">
					</picture>
				</h2>
			</li>
			<li class="category">
				<ul>
					<li>
						<a href="booth/food" title="食からさがす 電波学園祭2017">
							<picture>
								<source type="image/webp" srcset="img/food.webp">
								<img src="img/food.png" alt="食からさがす">
							</picture>
						</a>
					</li>
					<li>
						<a href="booth/learn" title="学からさがす 電波学園祭2017">
							<picture>
								<source type="image/webp" srcset="img/learn.webp">
								<img src="img/learn.png" alt="学からさがす">
							</picture>
						</a>
					</li>
					<li>
						<a href="booth/play" title="娯からさがす 電波学園祭2017">
							<picture>
								<source type="image/webp" srcset="img/play.webp">
								<img src="img/play.png" alt="娯からさがす">
							</picture>
						</a>
					</li>
				</ul>
			</li>
			<li class="class">
				<ul>
					<li>
						<a href="booth/med" title="先端医療系 電波学園祭2017">
							<picture>
								<source type="image/webp" srcset="img/med.webp">
								<img src="img/med.png" alt="先端医療系">
							</picture>
						</a>
					</li>
					<li>
						<a href="booth/info" title="先端技術系 電波学園祭2017">
							<picture>
								<source type="image/webp" srcset="img/info.webp">
								<img src="img/info.png" alt="先端技術系">
							</picture>
						</a>
					</li>
					<li>
						<a href="booth/electro" title="電子・電気系 電波学園祭2017">
							<picture>
								<source type="image/webp" srcset="img/electro.webp">
								<img src="img/electro.png" alt="電子・電気系">
							</picture>
						</a>
					</li>
				</ul>
			</li>
			<?php /*echo'
			<li>
				<a href="floor/" title="学内マップ 電波学園祭2017">
					<picture>
						<source type="image/webp" srcset="img/floor_banner.webp">
						<img class="floor_banner" src="img/floor_banner.png" alt="2017年電波学園祭 学内マップ">
					</picture>
				</a>
			</li>';*/ ?>
		</ul>
		<?php /*echo'
		<div class="event">
			<h2>
				<picture>
					<source type="image/webp" srcset="img/event_caption.webp">
					<img class="caption" src="img/event_caption.png" alt="電波学園祭イベント">
				</picture>
			</h2>
			<a href="#" title="学科対抗クリスマスツリーコンテスト 電波学園祭2017">
				<picture>
					<source type="image/webp" srcset="img/xmas_banner.webp">
					<img class="xmas_banner" src="img/xmas_banner.png" alt="学科対抗クリスマスツリーコンテスト">
				</picture>
			</a>
		</div>
		<div class="list">
			<h2>
				<picture>
					<source type="image/webp" srcset="img/news_caption.webp">
					<img class="caption" src="img/news_caption.png" alt="最新情報">
				</picture>
			</h2>
			<a href="#">
				<div class="list_content">
					<div class="list_img">
						<img src="holder.js/640x360?text=news01&bg=aaffaa" alt="">
					</div>
					<h3>タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</h3>
						<time datetime="2017-10-28 13:02">2017年10月28日 13:02</time>
				</div>
			</a>
			<a href="#">
				<div class="list_content">
					<div class="list_img">
						<img src="holder.js/640x360?text=news01&bg=aaffaa" alt="">
					</div>
					<h3>タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</h3>
						<time datetime="2017-10-28 13:02">2017年10月28日 13:02</time>
				</div>
			</a>
			<a href="#">
				<div class="list_content">
					<div class="list_img">
						<img src="holder.js/640x360?text=news01&bg=aaffaa" alt="">
					</div>
					<h3>タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</h3>
						<time datetime="2017-10-28 13:02">2017年10月28日 13:02</time>
				</div>
			</a>
			<a href="#">
				<picture>
				 	<source media="(max-width: 426px)" srcset="img/news_archives_sp.png">
					<img class="other" src="img/news_archives.png" alt="その他の記事を読む">
				</picture>
			</a>
		</div>';*/
?>
		<div class="event">
			<h2>
				<picture>
					<source type="image/webp" srcset="img/event_time_schedule.webp">
					<img class="caption" src="img/event_time_schedule.png" alt="イベントタイムスケジュール">
				</picture>
				<div class="schedule">
					<img src="img/time_schedule.png" alt="">
				</div>
			</h2>
		</div>
		<div class="other">
			<h2>
				<picture>
					<source type="image/webp" srcset="img/other.webp">
					<img class="caption" src="img/other.png" alt="その他">
				</picture>
			</h2>
			<div class="other_event">
				<a href="postercontest/"><img src="img/pscn.jpg" alt="第6回!! ウェブ・メディア科主催 ポスターコンテスト"></a>
				<a href="archives/"><img src="img/pnf.jpg" alt="過去のパンフレット一覧"></a>
			</div>
		</div>
		<?php }elseif($request[0]=='booth'){
			//ブース
			include_once('page/booth.php');
		}elseif($request[0] == 'access'){
			//アクセス
			include_once('page/access.php');
		}elseif($request[0] == 'about'){
			//このサイトについて
			include_once('page/about.php');
		}elseif($request[0] == 'archives'){
			//過去のパンフレット表紙
			include_once('page/archives.php');
		}elseif($request[0] == 'postercontest'){
			//ポスターコンテスト
			include_once('page/postercontest.php');
		}elseif($request[0] == 'floor'){
			//学内マップ
			include_once('page/floor.php');
		}
		?>
		<?php include_once('share.php');?>
		<?php	include_once('footer.php');?>
	</div>
	<?php include_once('script.php');?>
	<?php include_once('analytics.php');?>
</body>

</html>