<?php
try {
	// 接続
	$dbh = new PDO( 'sqlite:sqlite/tec.db' );
	$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$dbh->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

	$sql = 'select distinct booth.id, title, img, floor, class.name, short_content from booth, class, category where '.$flg[0].' = '.$flg[1].' and '.$flg[2].' = '.$flg[3].' and '.$flg[4].' = ? ORDER BY RANDOM()';
	$data[] = $request[ 1 ];

	$noimg = 'holder.js/320x320?text=Sorry... No Image. \n 画像準備中';
	
	$stmt = $dbh->prepare( $sql );
	$stmt->execute( $data );
} catch ( Exception $e ) {
	echo $e->getMessage();
}
?>
<h2>
	<picture>
		<source type="image/webp" srcset="../img/about.webp">
		<img class="caption" src="../about.png" alt="学内マップ">
	</picture>
</h2>
<div class="floor_map">
	<div class="content">
		<div class="floor_img">				
			<img src="../img/floor/floor.png" alt="1号館・3号館">
		</div>
		<h2><img src="../img/floor/map_learn.png" alt="学術系"></h2>
	</div>
</div>