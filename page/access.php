<h2>
	<picture>
		<source type="image/webp" srcset="<?php echo $path;?>img/access.webp">
		<img class="caption" src="<?php echo $path;?>img/access.png" alt="学校までのアクセス">
	</picture>
</h2>
<div class="access">
	<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12955.095430037987!2d139.7170482!3d35.7317798!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x89f2c0ef0e9ee79f!2z5a2m5qCh5rOV5Lq66Zu75rOi5a2m5ZyS5p2x5Lqs6Zu75a2Q5bCC6ZaA5a2m5qCh!5e0!3m2!1sja!2sjp!4v1506995527451" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="access_info">
		<p>住所 : 〒170-8418　東京都豊島区東池袋3-6-1</p>
		<p>アクセス : JR/東武東上線／西武池袋線／東京メトロ丸の内線・有楽町線・副都心線<br>池袋駅東口より徒歩 5～8分</p>
		<p><span>車でのご来場はご遠慮ください</span></p>
	</div>
</div>