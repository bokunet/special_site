<?php
try {
	// 接続
	$dbh = new PDO( 'sqlite:sqlite/tec.db' );
	$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$dbh->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
	if ( $request[ 1 ] == 'med' ||
		$request[ 1 ] == 'info' ||
		$request[ 1 ] == 'electro' ||
		$request[ 1 ] == 'food' ||
		$request[ 1 ] == 'learn' ||
		$request[ 1 ] == 'play' ) {
		$flg[ 0 ] = 'booth.class';
		$flg[ 1 ] = 'class.id';
		$flg[ 2 ] = 'booth.category';
		$flg[ 3 ] = 'category.id';
		if ( $request[ 1 ] == 'med' || $request[ 1 ] == 'info' || $request[ 1 ] == 'electro' ) {
			$flg[ 4 ] = 'class.path';
		} else {
			$flg[ 4 ] = 'category.path';
		}
		$sql = 'select distinct booth.id, title, img, floor, class.name, short_content from booth, class, category where '.$flg[0].' = '.$flg[1].' and '.$flg[2].' = '.$flg[3].' and '.$flg[4].' = ? ORDER BY RANDOM()';
		$data[] = $request[ 1 ];
	} else {
		$noimg = 'holder.js/320x320?text=Sorry... No Image. \n 画像準備中';
		//sql文
		$sql = 'select booth.id, title, img, area, floor, short_content, content, class.name from booth, class where booth.class = class.id and booth.id = ?';
		$data[] = $request[ 1 ];
	}
	$stmt = $dbh->prepare( $sql );
	$stmt->execute( $data );
} catch ( Exception $e ) {
	echo $e->getMessage();
}
?>
<h2>
	<picture>
		<source type="image/webp" srcset="../../img/find_caption.webp">
		<img class="caption" src="../../img/find_caption.png" alt="ブースをさがす">
	</picture>
</h2>
<?php

if ( $request[ 1 ] == 'med' ||
	$request[ 1 ] == 'info' ||
	$request[ 1 ] == 'electro' ||
	$request[ 1 ] == 'food' ||
	$request[ 1 ] == 'learn' ||
	$request[ 1 ] == 'play' ) {
	echo '<div class="booth_list">';
	while ( true ) {
		$rec = $stmt->fetch( PDO::FETCH_ASSOC );
		if ( $rec == false ) {
			break;
		}
		echo '<a href="../' . $rec[ 'id' ] . '"><div class="list_content">';
		echo '<div class="list_img">';
		if ( isset( $rec[ 'img' ] ) ) {
			echo '<img src="holder.js/640x360?text=news01&bg=aaffaa" alt="">';
		} else {
			echo '<img src="../../img/' . $request[ 1 ] . '.png">';
		}
		echo '</div>';
		echo '<h3>' . $rec[ 'title' ] . '</h3>';
		echo '<p><img class="place" src="../../img/place.png">' . $rec[ 'floor' ] . '</p>';
		echo '<p>・' . $rec[ 'name' ] . '</p>';
		echo '<p>' . $rec[ 'short_content' ] . '</p>';
		echo '<div class="button">詳細ページへ</div>';
		echo '</div></a>';
	}
	echo '</div>';
} else {
	$rec = $stmt->fetch( PDO::FETCH_ASSOC );
	echo '<article class="list_content">';
	echo '<title>' . $rec[ 'title' ] . '</title>';
	echo '<h2>' . $rec[ 'title' ] . '<span class="class"> - ' . $rec[ 'name' ] . '</span></h2>';
	//画像
	echo '<div class="content_flex">';
	if ( isset( $rec[ 'img' ] ) ) {
		echo '<div><img src="../img/' . $rec[ 'img' ] . '"></div>';
	} else {
		echo '<div><img src="' . $noimg . '" id="noimg"></div>';
	}
	//号館・フロア
	echo '<div><img src="../../img/floor/' . $rec[ 'area' ] . '.jpg"></div>';
	echo '</div>';
	//コンテンツ
	echo '<p><img class="place" src="../../img/place.png">' . $rec[ 'floor' ] . '</p>';
	if ( isset( $rec[ 'content' ] ) ) {
		echo '<p>' . $rec[ 'content' ] . '</p>';
	} else {
		echo '<p>' . $rec[ 'short_content' ] . '</p>';
	}
	echo '</article>';
}
?>