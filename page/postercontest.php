<h2>
	<picture>
		<source type="image/webp" srcset="../img/pscn_caption.webp">
		<img class="caption" src="../img/pscn_caption.png" alt="ポスターコンテスト">
	</picture>
</h2>
<div class="about">
	<picture>
		<!--source type="image/webp" srcset="../img/about_main.webp">-->
		<img src="../img/pscn_main.jpg" alt="ようこそTECパークへ">
	</picture>
	<div class="content">
		<p>今年の学園祭テーマ「ようこそTECパークへ」をもとに学生達がポスター制作を行いました。</p>
		<p>数ある作品の中からどのポスターが一番魅力的で、コンセプトが伝わってくるでしょうか？</p>
		<p>皆様の投票により結果が決まりますので、投票のご協力よろしくお願いします！</p>
		<p>場所は新1号館13階にある第10PC実習室にて開催されます。ぜひお立ち寄りください！</p>
	</div>
</div>