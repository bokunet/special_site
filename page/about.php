<h2>
	<picture>
		<source type="image/webp" srcset="../img/about.webp">
		<img class="caption" src="../about.png" alt="このサイトについて">
	</picture>
</h2>
<div class="about">
	<picture>
		<source type="image/webp" srcset="../img/about_main.webp">
		<img src="../about_main.png" alt="ようこそTECパークへ">
	</picture>
	<div class="content">
		<p>2017年10月28日(土)・29日(日)に東京電子専門学校では電波学園祭が行われます。</p>
		<p>それに伴い、昨年に引き続き、「電波学園祭2017特設サイト」を開設しました。</p>
		<p>本サイトの制作・運営はウェブ・メディア科の学生がおこなっています。</p>
		<p>学園祭当日の様子も当日レポートとして写真等をアップロードして更新していく予定です。</p>
	</div>
</div>