<article>
	<h2>
		<picture>
			<source type="image/webp" srcset="<?php echo $path;?>img/special73.webp">
			<img class="caption" src="<?php echo $path;?>img/special73.png" alt="今年発掘された学園祭パンフレット">
		</picture>
	</h2>
	<div class="archives">
		<p>現在解体工事中の2号館（2019年に新2号館完成予定）から1973年、第4回目の学園祭パンフレットが発掘されました！</p>
		<p>今回はそのパンフレットの表紙だけでなく、内容も一部公開します！</p>
		<div class="swiper-container2">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<img src="../img/archives/73-1.jpg">
				</div>
				<div class="swiper-slide">
					<img src="../img/archives/73-2.jpg">
				</div>
				<div class="swiper-slide">
					<img src="../img/archives/73-3.jpg">
				</div>
				<div class="swiper-slide">
					<img src="../img/archives/73-4.jpg">
				</div>
			</div>
			<div class="swiper-pagination"></div>
			<!-- Add Arrows -->
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
	</div>
</article>
<article>
	<h2>
		<picture>
			<source type="image/webp" srcset="<?php echo $path;?>img/archives.webp">
			<img class="caption" src="<?php echo $path;?>img/archives.png" alt="電波祭の歴史を辿るパンフレットデザイン">
		</picture>
	</h2>
	<div class="content_flex archives">
		<p>このページは、歴代のパンフレットを見ながら、ベテランの先生方や職員の方に当時を語っていただくコンテンツです。 特に東京電子専門学校OBの方は必見のコンテンツ。学園祭前日まで随時更新して行く予定です。</p>
		<p><strong>このページに無いパンフレットをお持ちの方はご一報をおねがいします！！</strong></p>
		<div>
			<p>2016年</p>
			<img src="../img/archives/2016.jpg" alt="">
			<p>高度情報システム科<br>江森 瑠璃子さん作</p>
		</div>
		<div>
			<p>2015年</p>
			<img src="../img/archives/2015.jpg" alt="">
			<p>ウェブ・メディア科<br>小坂 一泰さん作</p>
		</div>

		<div>
			<p>2014年</p>
			<img src="../img/archives/2014.jpg" alt="">
			<p>ウェブ・メディア科<br>久世 陽子さん作</p>
		</div>
		<div>
			<p>2013年</p>
			<img src="../img/archives/2013.jpg" alt="">
			<p>ウェブ・メディア科<br>宮澤 詩織さん作</p>
		</div>
		<div>
			<p>2012年</p>
			<img src="../img/archives/2012.jpg" alt="">
			<p>ウェブ・メディア科<br>石井 貴裕さん作</p>
		</div>
		<div>
			<p>2011年</p>
			<img src="../img/archives/2011.jpg" alt="">
			<p>ウェブ・メディア科<br>本間 圭一さん作</p>
		</div>
		<div>
			<p>2010年</p>
			<img src="../img/archives/2010.jpg" alt="">
			<p>高橋 雄也さん作</p>
		</div>
		<div>
			<p>2009年</p>
			<img src="../img/archives/2009.jpg" alt="">
			<p>船橋 実里さん作</p>
		</div>
		<div>
			<p>2008年</p>
			<img src="../img/archives/2008.jpg" alt="">
			<p>船橋 実里さん作</p>
		</div>
		<div>
			<p>2007年</p>
			<img src="../img/archives/2007.jpg" alt="">
			<p>星 宏樹さん作</p>
		</div>
		<div>
			<p>2006年</p>
			<img src="../img/archives/2006.jpg" alt="">
			<p>隈川 祐樹さん作</p>
		</div>
		<div>
			<p>2005年</p>
			<img src="../img/archives/2005.jpg" alt="">
			<p>寺田 理恵さん作</p>
		</div>
		<div>
			<p>2004年</p>
			<img src="../img/archives/2004.jpg" alt="">
			<p>野口 照喜さん作</p>
		</div>
		<div>
			<p>2003年</p>
			<img src="../img/archives/2003.jpg" alt="">
			<p>坂本 明希さん作</p>
		</div>
		<div>
			<p>2002年</p>
			<img src="../img/archives/2002.jpg" alt="">
			<p>野口 智子さん作</p>
		</div>
		<div>
			<p>2001年</p>
			<img src="../img/archives/2001.jpg" alt="">
			<p>情報処理科<br>齋藤 千枝子さん作</p>
		</div>
		<div>
			<p>2000年</p>
			<img src="../img/archives/2000.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1999年</p>
			<img src="../img/archives/wanted-204x300.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1998年</p>
			<img src="../img/archives/1998.jpg" alt="">
			<p>臨床検査学科<br>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1997年</p>
			<img src="../img/archives/1997.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1996年</p>
			<img src="../img/archives/1996.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1995年</p>
			<img src="../img/archives/wanted-204x300.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1994年</p>
			<img src="../img/archives/wanted-204x300.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1993年</p>
			<img src="../img/archives/1993.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1992年</p>
			<img src="../img/archives/1992.jpg" alt="">
			<p>和田 好司さん作</p>
		</div>
		<div>
			<p>1991年</p>
			<img src="../img/archives/wanted-204x300.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1990年</p>
			<img src="../img/archives/1990.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1989年</p>
			<img src="../img/archives/wanted-204x300.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1988年</p>
			<img src="../img/archives/1988.jpg" alt="">
			<p>望月 規央さん作</p>
		</div>
		<div>
			<p>1987年</p>
			<img src="../img/archives/wanted-204x300.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1986年</p>
			<img src="../img/archives/1986.jpg" alt="">
			<p>xxxxxxxxxさん作</p>
		</div>
		<div>
			<p>1985年</p>
			<img src="../img/archives/1985.jpg" alt="">
			<p>服部 健一さん作</p>
		</div>
		<div class="p_large">
			<p>1984年以前</p>
			<img id="p_last" src="../img/archives/wanted-204x300.png" alt="1984年以前のポスター・パンフレット表紙募集中">
		</div>
	</div>
</article>
<script>
var swiper2 = new Swiper('.swiper-container2', {
	pagination: '.swiper-pagination',
	paginationClickable: true,
	loop: true,
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
});
</script>