<?php
session_start();
session_regenerate_id(true);
if($_SESSION['login'] != 1){
	header('location:index.php');
	exit();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style.css">
<title>ダッシュボード</title>
<script src="tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
  selector: 'textarea',
  height: 500,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});
    </script>
<style>

</style>
</head>
<body>
<header>
	<p>ログイン : <?php echo $_SESSION['id'];?></p>
	<p><a href="logout.php">ログアウト</a></p>
</header>
<nav>
	<ul>
		<li><a href="#">ブース</a></li>
		<li><a href="#">最新記事</a></li>
		<li><a href="#">サンプル</a></li>
		<li><a href="#">サンプル</a></li>
		<li><a href="#">サンプル</a></li>
		<li><a href="#">サンプル</a></li>
		<li><a href="#">サンプル</a></li>
	</ul>
</nav>
<div style="height: 40px"></div>
<div class="wrapper">
	<form action="" method="post">
		<textarea name="aaa" id="foo" cols="30" rows="10">aaaaa</textarea>
	</form>
</div>
   
</body>
</html>