<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>無題ドキュメント</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style.css">
<script src="tinymce/tinymce.js"></script>
<script src="jquery-3.2.1.min.js"></script>
<script>
tinymce.init({
	selector: "textarea", // id="foo"の場所にTinyMCEを適用
	plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
});
</script>
<style>
	body{
		padding: .5rem;
	}
</style>
</head>

<body>
<form action="" method="post"></form>
<button>Clone Me!</button>
<script>
var count = 0;
var temp = '<input type="file"><textarea></textarea><input type="file"><br>';
$("button").click(function(){
	count++;
  $(temp).appendTo("form").attr('name',count);
	tinymce.init({
	selector: "textarea", // id="foo"の場所にTinyMCEを適用
	plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
});
});
	
</script>
</body>
</html>