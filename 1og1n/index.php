<?php
session_start();
session_regenerate_id(true);

$result = '';
try {
	if(isset($_POST['submit'])){
		$_POST['pass']=md5($_POST['pass']);
		// 接続
		$dbh = new PDO('sqlite:../sqlite/tec.db');
		// SQL実行時にもエラーの代わりに例外を投げるように設定
		// (毎回if文を書く必要がなくなる)
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// デフォルトのフェッチモードを連想配列形式に設定 
		// (毎回PDO::FETCH_ASSOCを指定する必要が無くなる)
		$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$sql = 'select id, count(*) from user where id = ? and pass = ?';
		$stmt = $dbh->prepare($sql);
		$data[]=$_POST['id'];
		$data[]=$_POST['pass'];
		$stmt->execute($data);
		// 挿入（プリペアドステートメント）
		//$stmt = $pdo->prepare("INSERT INTO fruit(name, price) VALUES (?, ?)");
		//foreach ([['りんご', '200'], ['バナナ', '200']] as $params) {
		//    $stmt->execute($params);

		//$rec = $stmt->fetchAll();//複数のsqlで出力する際にはこっちのほうが便利かも
		$rec = $stmt->fetch(PDO::FETCH_ASSOC);//複数のsql文使わないならこっちでいい
		if($rec['count(*)']==0){
			$result = '<small>ユーザーメイトパスワードを確認の上、再度入力をしてください。</small>';
		}else{
			$_SESSION['id']=$rec['id'];
			$_SESSION['login']=1;
			header('location:admin.php');
			exit();
		}
		
	}
}catch (Exception $e){
	echo $e->getMessage();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ログイン</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style.css">
<style>
	.main{
		display: flex;
		justify-content: center;
		align-items: center;
	}
	form{
		background: #fff;
		display: block;
		padding: 2rem;
		width: 100%;
	}
	h1{
		font-size: 1.2rem;
		font-weight: bold;
		margin-bottom: 1rem;
	}
	small{
		display: block;
		text-decoration: underline;
		margin-bottom: .5rem;
		font-size: .8rem;
		color: #476EA3;
	}
	input{
		display: block;
		margin-bottom: 2rem;
		border-radius: .5rem;
		padding: .5rem;
		border: solid 1px #666;
	}
	input:focus,button{
		outline: none;
	}
	input:last-of-type{
		margin-bottom: 0;
	}
	button{
		background: none;
		border: solid 1px #666;
		padding: .5rem;
		border-radius: .5rem;
		cursor: pointer;
	}
</style>
</head>

<body>
<div class="main">
	<form method="post">
		<?php echo $result;?>
		<h1>ログイン画面</h1>
		<input type="text" name="id" placeholder="ユーザー名" required>
		<input type="password" name="pass" placeholder="パスワード" required>
		<input type="submit" name="submit" value="ログイン">
	</form>
</div>
</body>
</html>
