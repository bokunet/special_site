<?php
session_start();
session_regenerate_id(true);
if($_SESSION['login'] != 1){
	header('location:index.php');
	exit();
}
try {
	// 接続
	$dbh = new PDO('sqlite:../sqlite/tec.db');
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$sql = 'select id,title from booth';
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	// 挿入（プリペアドステートメント）
	//$stmt = $pdo->prepare("INSERT INTO fruit(name, price) VALUES (?, ?)");
	//foreach ([['りんご', '200'], ['バナナ', '200']] as $params) {
	//    $stmt->execute($params);
	//$rec = $stmt->fetchAll();//複数のsqlで出力する際にはこっちのほうが便利かも
	//複数のsql文使わないならこっちでいい
}catch (Exception $e){
	echo $e->getMessage();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>無題ドキュメント</title>
<link rel="stylesheet" href="style.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
	body{
		padding: .5rem;
	}
	details{
		display: block;
		margin-bottom: 1rem;
	}
	details ul{
		padding: 1rem 0 1rem 2rem;
	}
	summary{
		padding: .5rem;
		background: #ccc;
		cursor: pointer;
		user-select:none;
	}
</style>
</head>

<body>
<section>
  <details>
    <summary><span>最新情報のページ</span></summary>
    <ul>
    <?php
		while(true){
			$rec = $stmt->fetch(PDO::FETCH_ASSOC);
			if($rec==false){
				break;
			}
			echo '<li><a target="_parent" href="edit.php?id='.$rec['id'].'">'.$rec['title'].'</a>';
		}
		?>
    </ul>
  </details>
  <details>
    <summary><span>ブースのページ</span></summary>
    <ul>
    	<li>a</li>
    </ul>
  </details>
</section>
</body>
</html>
