<meta charset="UTF-8">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MFDCK6F');</script>
<!-- End Google Tag Manager -->
<title><?php echo $title;?>電波学園祭2017特設サイト</title>
<meta name="Description" content="<?php echo $description;?>">
<meta name="theme-color" content="#dda467">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo $path; ?>style/style.css">
<link rel="stylesheet" href="<?php echo $path; ?>style/swiper.css">
<link href="https://fonts.googleapis.com/earlyaccess/sawarabigothic.css" id="gwebfont">
<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
<script src="<?php echo $path; ?>js/swiper.min.js"></script>
<link rel="icon" href="<?php echo $path;?>img/icon/16x16.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $path;?>img/icon/32x32.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $path;?>img/icon/64x64.ico" type="image/x-icon">
<!--develop-->
<script src="<?php echo $path; ?>js/holder.min.js"></script>
<!--end_develop-->